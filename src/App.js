import logo from './logo.svg';
import './App.css';
import Content from './Content';
import React, { useEffect, useState } from 'react';
import Clock from 'react-clock';
import 'react-clock/dist/Clock.css';

function App() {
    const [value, setValue] = useState(new Date());
    console.log('value', value)
  
    useEffect(() => {
      console.log('Inside useEffect');
      const interval = setInterval(() => setValue(new Date()), 1000);
  
      return () => {
        clearInterval(interval);
      };
    }, []);

    useEffect(() => {
      console.log('Value has changed', value);
    }, [value]);

  return ( 
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
          <Content />
          <Clock value={value}/>
      </header>
    </div>
  );
}

export default App;
